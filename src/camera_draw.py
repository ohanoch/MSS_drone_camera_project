#!/usr/bin/python

# Made by Or Hanoch and Meir Rosendorff
# Project Manager: Shunmuga Pillay
# Proffesional Adviser: Pravesh Ranchod

# In this program we use data from Optitrack Motive with Flex13 cameras to track a drone.
# We transfering the data from the Windows 7 PC to the Ubuntu PC running ROS using vrpn_client_ros.
# This code takes the location data and uses it to determine how to fly a drone.

# importing ros messages
import rospy
from geometry_msgs.msg import PoseStamped

# import miscellaniouse python libraries used within the code
import time
import datetime
import sys
import math

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

RATE_CONST = 30

all_pose_x = []
all_pose_y = []
all_pose_z = []
pose_count = 0
start_time = time.time()

def store_pose(data):
	global pose_count
	global all_pose

	if pose_count%2 == 0:
		print(str(data.pose.position) + " added to pose")
		all_pose_x.append(data.pose.position.x)
		all_pose_y.append(data.pose.position.y)
		all_pose_z.append(data.pose.position.z)
	pose_count += 1

def draw():
	# initialize ros node and subscribe to topics
	rospy.init_node('painter', anonymous=True)

	# Subscribe to each drone's vrpn
	rospy.Subscriber("/vrpn_client_node/wand/pose", PoseStamped, store_pose)
	print("subscribed to node " + "/vrpn_client_node/wand/pose")

	# set ros loop rate
	rate = rospy.Rate(RATE_CONST)
	
	# start ROS loop
	while (not rospy.is_shutdown()):

		if time.time() - start_time > 5:
			break

		rate.sleep()

	print("length of all pose: " + str(len(all_pose_x)))
	#print(all_pose)
	fig = plt.figure()
	ax = fig.add_subplot(111, projection='3d')
	ax.scatter(all_pose_x,all_pose_y,all_pose_z, c='r', marker='o')

	plt.show()

def usage():
    print("No parameters must be entered.\n\n")


if __name__ == '__main__':
    print("Drone movement by location data from Optitrack Motive initialized.\n\n")
    now = datetime.datetime.now()
    print(
        "time started: " + str(now.day) + "/" + str(now.month) + "/" + str(now.year) + " at: " + str(
            now.hour) + ":" + str(
            now.minute) + ":" + str(now.second) + "\n")
    if len(sys.argv) != 1:
        usage()
        sys.exit(1)

    try:
        draw()

    except rospy.ROSInterruptException:
        print('Ran into error - forcing landing and shutdown.')
        sys.exit(1)
