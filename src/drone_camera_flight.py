#!/usr/bin/python

# Made by Or Hanoch and Meir Rosendorff
# Project Manager: Shunmuga Pillay
# Proffesional Adviser: Pravesh Ranchod

# In this program we use data from Optitrack Motive with Flex13 cameras to track a drone.
# We transfering the data from the Windows 7 PC to the Ubuntu PC running ROS using vrpn_client_ros.
# This code takes the location data and uses it to determine how to fly a drone.
# Here we do a proof of concept of flying a drone in a square while changing orientation (spin) of drone.

# This could be expanded to controlling multiple drones by changeing the NUM_DRONES global variable
# and changing the move command to take the other drones into account

# importing ros messages
from std_msgs.msg import Empty
import rospy
from geometry_msgs.msg import Twist
from geometry_msgs.msg import PoseStamped

# importing python libraries required for computation
from tf.transformations import euler_from_quaternion


# import miscellaniouse python libraries used within the code
import time
import datetime
import sys
import math

# Globals
RATE_CONST = 30

# PID constants
# used following link to help understand how to tune them:
# https://electronics.stackexchange.com/questions/127524/how-to-calculate-kp-kd-and-ki
Kp = 1			# how much target gets taken into account
Ki = 0			# how much past moves get negated (thats why Ki is negative)
Kd = 0.05		# how much changes in targets get taken into account

# Speed variables to be given at certain points.
# NOTE: Up and Down need specific speeds - presumably because of gravity
GENERAL_SPEED = 0.025
UP_SPEED = 0.1			# speeds for general movement between tags
DOWN_SPEED = 0.03		# speed for moving down
SPIN_SPEED = 0.3		# speed for spinning

GRAVITY_CONST = 0.02  # speed to maintain height and fight gravity. might not be necessary.

# Ristrictions due to camera placement - need to be change according to setup!
MAX_X = 0.86
MAX_Y = 1.24
MAX_Z = 0.58
MIN_X = -0.62
MIN_Y = 0.07 
MIN_Z = -0.62
BUFFER_SIZE = 0.1
SPIN_BUFFER_SIZE = 0.225


class Drone:
	def __init__(self, name):
		# Current drone movement speed variables
		self.name = name		# defined in launch file. for drone1 name = ardrone1; drone2 name = ardrone2; etc
								# Look at ardrone# of remmapings before the drone node (identifiable by ip address)
								# Make sure that the rigid body name given in Motive corresponds to this name!

		self.v_direction = 0		# vertival direction (up down) up = 1, down = -1
		self.h_direction = 0		# horizontal direction (left right) left = 1, right = -1
		self.f_direction = 0		# forward direction (forward backward)
		self.roll = 0		# forward/backward flip
		self.pitch = 0		# side to side swivle
		self.yaw = 0		# left/right flip
		self.land = False		# Checks if a command to land has been given
		self.stabilization_counter = 0		# counts consecutive seconds of location on target point within buffer range
		self.movement_stage = 0		# stage number determining which movement maneuver we are in

		self.xyz_pid = Pid()		# Drone xyz movement to be done after pid has been calculated
		self.rpy_pid = Pid()		# Drone roll,pitch,yaw movement to be done after pid has been calculated
		self.vel_msg = Twist()		# Final movement command to be sent to drone (velocity messege to be published)

		self.velocity_publisher = None		# topic for publushing movements
		self.land_publisher = None			# topic for publishing land commands
		self.takeoff_publisher = None		# topic for publishing takeoff commands

		self.is_moving = False		# indicates if the drone is currently performing a movement command

	# Print the variables of the drones movement
	def print_movement_stats(self):
		print (self.name + " v_direction: " + str(self.v_direction) +
			self.name + " h_direction: " + str(self.h_direction) +
			self.name + " f_direction: " + str(self.f_direction) +
			self.name + " roll: " + str(self.roll) +
			self.name + " pitch: " + str(self.pitch) +
			self.name + " yaw: " + str(self.yaw))

	# reset the drone movement to nothering - aka hover
	def reset_movement(self):
		print ("reseting movement")
		self.v_direction = 0
		self.h_direction = 0
		self.f_direction = 0
		self.roll = 0
		self.pitch = 0
		self.yaw = 0

	# translate orientation as provided by optitrack into spinning (pitch) angles
	# from values ranging from -1 to 1
	# into values ranging from -pi to pi
	def get_angle(self, orientation):
		return orientation.y * orientation.w * math.pi

	# input: current angle of drone with relation to xyz of flying area
	#		 direction of flight with relation to fly area
	# output: a tuple of (f_direction, h_direction)
	#		  for the amount to fly forward and horizontal with relation to the drone
	# TODO: improve by using mod %
	def world_to_object_instructions(self, command, curr_angle):
		if command == "LEFT":
			if curr_angle >= 0:
				if curr_angle < math.pi / 2:
					return (math.cos(math.pi / 2 - curr_angle), math.sin(math.pi / 2 - curr_angle))
				else:
					return (math.cos(curr_angle - math.pi / 2), -1 * math.sin(curr_angle - math.pi / 2))
			else:
				if curr_angle > -1 * math.pi / 2:
					return (-1 * math.cos(math.pi / 2 + curr_angle), math.sin(math.pi / 2 + curr_angle))
				else:
					return (-1 * math.cos(-1 * math.pi / 2 - curr_angle), -1 * math.sin(-1 * math.pi / 2 - curr_angle))
					
		elif command == "RIGHT": #-1 * LEFT - can be merged?
			if curr_angle >= 0:
				if curr_angle < math.pi / 2:
					return (-1 * math.cos(math.pi / 2 - curr_angle), -1 * math.sin(math.pi / 2 - curr_angle))
				else:
					return (-1 * math.cos(curr_angle - math.pi / 2), math.sin(curr_angle - math.pi / 2))
			else:
				if curr_angle > -1 * math.pi / 2:
					return (math.cos(math.pi / 2 + curr_angle), -1 * math.sin(math.pi / 2 + curr_angle))
				else:
					return (math.cos(-1 * math.pi / 2 - curr_angle), math.sin(-1 * math.pi / 2 - curr_angle))

		elif command == "FORWARD":
			if curr_angle >= 0:
				if curr_angle < math.pi / 2:
					return (math.cos(curr_angle), -1 * math.sin(curr_angle))
				else:
					return (-1 * math.cos(math.pi - curr_angle), -1 * math.sin(math.pi - curr_angle))
			else:
				if curr_angle > -1 * math.pi / 2:
					return (math.cos(-1 * curr_angle), math.sin(-1 * curr_angle))
				else:
					return (-1 * math.cos(math.pi + curr_angle), math.sin(math.pi + curr_angle))

		elif command == "BACKWARD":
			if curr_angle >= 0:
				if curr_angle < math.pi / 2:
					return (-1 * math.cos(curr_angle), math.sin(curr_angle))
				else:
					return (math.cos(math.pi - curr_angle), math.sin(math.pi - curr_angle))
			else:
				if curr_angle > -1 * math.pi / 2:
					return (-1 * math.cos(-1 * curr_angle), -1 * math.sin(-1 * curr_angle))
				else:
					return (math.cos(math.pi + curr_angle), -1 * math.sin(math.pi + curr_angle))

	# move to the given point (array with x,y,z) with a buffer (given as a global variable)
	# with target orientation given in radians and 
	# spin_dir = -1 for closckwise rotation, spin_dir =  1 for counterclockwise, spin_dir = 0 only adjust
	def move_to_point(self, position, orientation, target_point, target_angle, spin_dir):
		print("----------- " + str(self.name) + " ENTERED move_to_point with movement stage: " + str(self.movement_stage) + "--------------")
		print("moving from " + str(position.x) + "," + str(position.y) + "," + str(position.z) + " towards " + str(target_point))
		curr_angle = self.get_angle(orientation)
		print("current orientation: " + str(orientation.y) + " in radians: " + str(curr_angle) + " going for angle " + str(target_angle))

		# determine spinning of drone
		if target_angle < curr_angle + (3/2) * SPIN_BUFFER_SIZE and\
			target_angle > curr_angle + SPIN_BUFFER_SIZE/2:
			print("adjusting spinning COUNTER-CLOCKWISE")
			self.pitch = SPIN_SPEED * (target_angle - curr_angle)
		elif target_angle > curr_angle - (3/2) * SPIN_BUFFER_SIZE and\
			target_angle < curr_angle - SPIN_BUFFER_SIZE/2:
			print("adjusting spinning CLOCKWISE")
			self.pitch = SPIN_SPEED * (target_angle - curr_angle)
		elif target_angle > curr_angle + (3/2) * SPIN_BUFFER_SIZE or\
			target_angle < curr_angle - (3/2) * SPIN_BUFFER_SIZE:
			if spin_dir > 0:
				print("spinning from request COUNTER-CLOCKWISE")
			else:
				print("spinning from request CLOCKWISE")
			self.pitch = spin_dir * GENERAL_SPEED#SPIN_SPEED
		else:
			self.pitch = 0
		
		#NOTE: moving "left" "right" "forward: "backwards"  refers to  world coordinates
		# but moving instructions (h/f_direction) is done in object (drone) coordinates
		# Thus we need to translate the instructions from world coordinates to object coordinates
		# We do this using the world_to_object_instructions function
		# As we only take spinning (pitch) into accoutn, and not flipping
		# going up and down does not need to be altered

		f_move = 0
		h_move = 0
		self.f_direction = 0
		self.h_direction = 0

		if position.x <= target_point[0] - BUFFER_SIZE/3:
			print("moving LEFT")
			f_move, h_move = self.world_to_object_instructions("LEFT", curr_angle)
		elif position.x >= target_point[0] + BUFFER_SIZE/3:
			print("moving RIGHT")
			f_move, h_move = self.world_to_object_instructions("RIGHT", curr_angle)

		print("f_moveLR: " + str(f_move) + " h_moveLR: " + str(h_move))
		self.f_direction += GENERAL_SPEED * min(1, abs(target_point[0] - position.x)) * f_move
		self.h_direction += GENERAL_SPEED * min(1, abs(target_point[0] - position.x)) * h_move

		if position.y <= target_point[1] - BUFFER_SIZE/3:
			print("moving UP")
			self.v_direction = UP_SPEED
		elif position.y >= target_point[1] + BUFFER_SIZE/3:
			print("moving DOWN")
			self.v_direction = -1 * DOWN_SPEED
		else:
			self.v_direction = GRAVITY_CONST

		f_move = 0
		h_move = 0
		if position.z <= target_point[2] - BUFFER_SIZE/3:
			print("moving FORWARD")
			f_move, h_move = self.world_to_object_instructions("FORWARD", curr_angle)
		elif position.z >= target_point[2] + BUFFER_SIZE/3:
			print("moving BACKWARD")
			f_move, h_move = self.world_to_object_instructions("BACKWARD", curr_angle)

		print("f_moveFB: " + str(f_move) + " h_moveFB: " + str(h_move))
		self.f_direction += GENERAL_SPEED * min(1, abs(target_point[2] - position.z)) * f_move
		self.h_direction += GENERAL_SPEED * min(1, abs(target_point[2] - position.z)) * h_move

		#determine if goal has been reached and act accordingly
		if position.x > target_point[0] - BUFFER_SIZE and \
			position.x < target_point[0] + BUFFER_SIZE and \
			position.y > target_point[1] - BUFFER_SIZE and \
			position.y < target_point[1] + BUFFER_SIZE and \
			position.z > target_point[2] - BUFFER_SIZE and \
			position.z < target_point[2] + BUFFER_SIZE and \
			curr_angle > target_angle - SPIN_BUFFER_SIZE and\
			curr_angle < target_angle + SPIN_BUFFER_SIZE:
			print("\n####################################################################")
			print("--- Reached point " + str(target_point) + " with buffer of " + str(BUFFER_SIZE) + " ---")
			print("####################################################################\n")
			
			# wait for 25 consecutive time steps of 0.01 of staying in goal range before changing move
			if self.stabilization_counter == 25:
				self.movement_stage += 1
				print("\n**************************************************************")
				print("movement_stage changed to " + str(self.movement_stage))
				print("**************************************************************\n")
				self.stabilization_counter = 0
			else:
				print("sleeping for 1 seconds to stabilize. stabilization_counter = " + str(self.stabilization_counter))
				self.stabilization_counter = self.stabilization_counter + 1
				rospy.sleep(0.01)
		else:
			self.stabilization_counter = 0

	# move drone according to current maneuver stage.
	# gets called when cameras detect drone (via sbscription to /vrpn_client_node/" + self.name + "/pose")
	# input: data - contains data.pose.position for xyz position and data.orientation for roll,oithc,yaw
	# output: summons appropriate maneuver function that will change drones direction variables
	# currently set to go in a square while turning 90 degrees between corners
	def move_drone(self, data):
		if not self.is_moving:
			self.is_moving = True	
			print("entered move_data")

			if self.movement_stage == 4:		#land
				self.land = True
				self.land_publisher.publish(Empty())
			if self.movement_stage == 0:		# go to top right corner don't change oritentation
				self.move_to_point(data.pose.position, data.pose.orientation, [MAX_X, MAX_Y, MIN_Z],  0, 0) 
			if self.movement_stage == 1:		# go to bottom right corner turn 90 degrees counterclockwise
				self.move_to_point(data.pose.position, data.pose.orientation, [MAX_X, MAX_Y, MAX_Z], -1*math.pi/2, -1)
			if self.movement_stage == 2:		# go to bottom left corner turn 90 degrees counterclockwise
				self.move_to_point(data.pose.position, data.pose.orientation, [MIN_X, MAX_Y, MAX_Z], math.pi, -1)
			if self.movement_stage == 3:		# go to top left corner turn 90 degrees counterclockwise
				self.move_to_point(data.pose.position, data.pose.orientation, [MIN_X, MAX_Y, MIN_Z], math.pi/2, -1)



	# Does all preperations for drones whe they are initialized. This includes:
	# setting up the publishers and subscribers
	# taking off
	# resetting all variables
	# setting initial movement speeds (go up)
	def preperation(self):
		# initialize ros node and subscribe to topics
		rospy.init_node('ardrone_' + self.name, anonymous=True)
		self.velocity_publisher = rospy.Publisher('/' + self.name + '/cmd_vel', Twist, queue_size=1)
		self.land_publisher = rospy.Publisher('/' + self.name + '/land', Empty, queue_size=10)
		self.takeoff_publisher = rospy.Publisher('/' + self.name + '/takeoff', Empty, queue_size=10)

		# Give 10 seconds for connection to be established + time to prepare screen capture and other things
		print("wait 10 seconds for takeoff...\n")
		sys.stdout.flush()
		rospy.sleep(10.)

		# send takeoff command
		print("sending takeoff command to " + '/' + self.name + '/takeoff' + "...\n")
		sys.stdout.flush()
		self.takeoff_publisher.publish(Empty())

		# Give drone time to stabalize after takeoff before sending new commands
		print("sleeping 12 seconds to stabalize...\n")
		sys.stdout.flush()
		rospy.sleep(12.)

		# initialize all the ros movement variables to 0
		reset_vel(self.velocity_publisher, self.vel_msg)

		# Subscribe to each drone's vrpn
		rospy.Subscriber("/vrpn_client_node/" + self.name + "/pose", PoseStamped, self.move_drone)
		print("subscribed to node " + "/vrpn_client_node/" + self.name + "/pose")

		# default to go up after takeoff until a tag is detected
		self.v_direction = UP_SPEED

class Pid:
    def __init__(self):
        self.last_target = [0, 0, 0]
        self.last_pid_time = 0
        self.error_sum = [0, 0, 0]

    # PID controller meant to stabalize the drone and avoid drifting and other unwanted movements
    # based off of eschnou's ardrone_autonomy prototype pid
    # https://github.com/eschnou/ardrone-autonomy/blob/master/lib/PID.js
    # input: target location to move to (1x3 list). Also uses the class's variables for past time,target and error sum
    # output: movement to be made in each direction (1x3 list)
    def apply_pid(self, target):
        curr_time = time.time()

        dt = (curr_time - self.last_pid_time)  # time difference since last pid calculation

        de = [0, 0, 0]  # error derivation
        if self.last_pid_time != 0:
            de = [(i - j) / dt for i, j in zip(target, self.last_target)]
            self.error_sum = [j + k for j, k in zip([i * dt for i in target], self.error_sum)]  # Integrate error

        final_movement = [a + b + c for a, b, c in
                          zip([Kp * i for i in target], [Ki * i for i in self.error_sum], [Kd * i for i in de])]

        self.last_target = target
        self.last_pid_time = curr_time

        return final_movement

# Initializing global drone objects
NUM_DRONES = 1
DRONES = []
for i in range(1,NUM_DRONES+1):
	DRONES.append(Drone("ardrone" + str(i)))

# reset movement of a Twist messege
def reset_vel(velocity_publisher, vel_msg):
    vel_msg.linear.x = 0.0
    vel_msg.linear.y = 0.0
    vel_msg.linear.z = 0.0
    vel_msg.angular.x = 0.0
    vel_msg.angular.y = 0.0
    vel_msg.angular.z = 0.0

    velocity_publisher.publish(vel_msg)

# Checks if all drones got landing commands
def all_landed():
	for drone in DRONES:
		if not drone.land:
			return False
	return True

def flight():

	#do preperation for all drones
	for drone in DRONES:
		drone.preperation()

	# set ros loop rate
	rate = rospy.Rate(RATE_CONST)
	
	# start ROS loop
	while (not rospy.is_shutdown()) and not all_landed():

		# Calculate movement for drones with PID
		for drone in DRONES:
			xyz_command = drone.xyz_pid.apply_pid([drone.f_direction, drone.h_direction, drone.v_direction])
			rpy_command = drone.rpy_pid.apply_pid([drone.roll, drone.yaw, drone.pitch])

			print("sending movement command to drone: " + str(drone.name))
			print("xyz_command: " + str(xyz_command))
			print("rpy_command: " + str(rpy_command))
			
			drone.vel_msg.linear.x = xyz_command[0]
			drone.vel_msg.linear.y = xyz_command[1]
			drone.vel_msg.linear.z = xyz_command[2]
			drone.vel_msg.angular.x = 0
			drone.vel_msg.angular.y = 0
			drone.vel_msg.angular.z = rpy_command[2]

		# Publish movement commands for drones
		for drone in DRONES:
			drone.velocity_publisher.publish(drone.vel_msg)
			drone.is_moving = False

		rate.sleep()

	# land all drones
	for drone in DRONES:
		drone.land = True
		drone.land_publisher.publish(Empty()) # land all drones - programm finished
			

def usage():
    print("No parameters must be entered.\n\n")


if __name__ == '__main__':
    print("Drone movement by location data from Optitrack Motive initialized.\n\n")
    now = datetime.datetime.now()
    print(
        "time started: " + str(now.day) + "/" + str(now.month) + "/" + str(now.year) + " at: " + str(
            now.hour) + ":" + str(
            now.minute) + ":" + str(now.second) + "\n")
    if len(sys.argv) != 1:
        usage()
        sys.exit(1)

    try:
        print("RATE_CONST: " + str(RATE_CONST))
        print("Kp: " + str(Kp))
        print("Ki: " + str(Ki))
        print("Kd: " + str(Kd))
        print("GENERAL_SPEED: " + str(GENERAL_SPEED))
        print("UP_SPEED: " + str(UP_SPEED))
        print("DOWN_SPEED: " + str(DOWN_SPEED))
        print("GRAVITY_CONST: " + str(GRAVITY_CONST))
        print("MAX_X: " + str(MAX_X))
        print("MAX_Y: " + str(MAX_Y))
        print("MAX_Z: " + str(MAX_Z))
        print("MIN_X: " + str(MIN_X))
        print("MIN_Y: " + str(MIN_Y))
        print("MIN_Z: " + str(MIN_Z))
        print("BUFFER_SIZE: " + str(BUFFER_SIZE))
        print("SPIN_BUFFER_SIZE: " + str(SPIN_BUFFER_SIZE))

        flight()

    except rospy.ROSInterruptException:
        print('Ran into error - forcing landing and shutdown.')
        sys.exit(1)
