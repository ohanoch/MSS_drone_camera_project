# Tracking AR-DRONE Using Optitrack Camera System

The goal of this project is to track drones using infra-red cameras and use the information to manuever the drones.

This was tested using ROS Kinetic on Ubuntu 16.04 as well as Optitrack Motive 2.0.1 and the vrpn_ros_client and ardrone_autonomy packages on ROS.

## Requirements
These are the system requirements things were tested on, some changes may be possible:
- AR-Drone (preferably v2)
- Ubuntu 16.04
- ROS Kinetic
- Windows 7 PC
- Router (tested using NetGear R7000)
- 3rd PC with wifi
- Tripods (or some other way of hanging cameras)
- Optitrack toolkit:
    - Optitrack Motive (with hardware dongle)
    - Flex 13 cameras
    - Optihub 2.0 x2
    - IR reflective markers
    - Calibration tools (Optiwand and calibration square)
    - Cables


## Preperation

- It is recommended to go through the ROS tutorials and install ROS Kinetic through them:
http://wiki.ros.org/ROS/Tutorials/
- Clone this repo into your catkin_ws/src folder
- Install ardrone_autonomy package for ROS:  
https://github.com/AutonomyLab/ardrone_autonomy
- Instructions on setting up the network using a NetGear R7000 can be found at:  
https://gitlab.com/ohanoch/MSS_Guides/blob/master/NetGear/netgear_nighthawk_router_setup_guide_for_multiple_ardrones.pdf
- Instructions on installing vrpn_ros_client ROS package as well as setting up Motive and everything else can be found at:  
https://gitlab.com/ohanoch/MSS_Guides/blob/master/OptiTrack/Tracking_objects_using_OptiTrack_ROS.pdf
- Instructions on setting up control of multiple drones can be found at:  
https://gitlab.com/ohanoch/MSS_Guides/blob/master/Drones/launching_multiple_ardrones_guide.pdf

## Included Files
- src/
    - drone_camera_flight.py  
        - This is the most updated code.
        - It includes code for instructing drone to go to a position and spin at the same time.
        - Intended to have a drone move in a square while wpinning by 90 degrees between corners
        - This code is still being tested and does not work well.
        - This is the cleanest and best commented code.
        - Usage (after launching drones):
        rosrun MSS_drone_camera_project drone_camera_flight.py
    - drone_camera_flight_poc_forward.py
        - This code is not update with spinning properly.
        - It is the most functional code. It successfully moves a drone in a square while always facing towards positive Z.
    - camera_draw.py
        - This is just proof of concept code that allows you to "draw" using the optiwand in the flyspace and then it will display a 3D graph of the drawing.
- launch/
    - launch_multi_drone.launch
        - This is the main launch file being used.
        - It accepts as input a name for the drone and it's IP address
        - It creates a node for the drone as well as creating separate ros topics for that drone using its specific name
        - Usage:
        roslaunch launch_multi_drone.launch drone_name:=ardrone1 droneip:=192.168.1.10
    - launch_multi_drone.sh
        - A bash script to launch multiple drones.
        - Calls launch_multi_drone.launch multiple times with consecutive drone names and IP addresses.
            - Drone names are "ardrone1", "ardrone2", "ardrone3", ...
            - IPs are "192.168.1.10", "192.168.1.11", "192.168.1.12", ...
        - Accepts as input the number of drones to launch.
        - Usage (for 2 drones):
        ./launch_multi_drone.sh 2
    - launch_single_drone.launch
        - Used for launching a single drone
        - Not recommended for use
        - Instead use launch_multi_drone.sh with an input of 1, i.e:
        ./launch_multi_drone.sh 1
- CMakeLists.txt and package.xml are used by ROS

## Licenses
This is distributed using GPLv3 License

## Credits
Developed originally for the University of the Witwatersrand, South Africa  
Made by Or Hanoch
Project Manager: Shunmuga Pillay
Professional Adviser: Pravesh Ranchod